# Simultaneously Optimizing Bidding Strategy in Pay-as-Bid-Markets and Production Scheduling

This software computes an optimal bidding strategy on a balancing market for energy-intensive processes that considers
marketing the remaining production flexibility on a Day-Ahead market. Two approaches to achieve this are an explicit
enumeration scheme and a MINLP-formulation.

#Bug reports, feature requests
Please email Tim.Varelmann@avt.rwth-aachen.de. 


Ensure that you have a working environment of GAMS with an MINLP solver. Also, an installation of MATLAB is required
to execute the main scripts. Both folders 'explicit enumeration approach' and 'MINLP approach' contain a MATLAB-script
whose name starts with MAIN, executing them computes a solution for an aluminum production process with market data
from early 2018 in Germany.
