function [m,b,PthLinMin,PthLinMax,meanae,maxae,maxre] = pwlf(Pth0,PthVar,eta0,etaVar,etaNodes)

    PthInt = [Pth0*(1-0.25), Pth0, Pth0*(1+0.25)];
    etaMin = eta0*(1-etaVar/(1+etaVar));
    eta = [etaMin, eta0, etaMin];
    coeff = polyfit(PthInt,eta,2);
    x = linspace(Pth0-30,Pth0+30,100);
    y = polyval(coeff,x);
    deriv = [3*coeff(1), 2*coeff(2), 1*coeff(3)];
    
    PthGrid = linspace(Pth0-PthVar,Pth0+PthVar,etaNodes);
    PthGrid = movmean(PthGrid,[0 1]);
    PthGrid = PthGrid(1:end-1);
    PthGrid = [PthGrid(1:end/2) Pth0 PthGrid(end/2+1:end)];
    PalGrid = polyval(coeff,PthGrid).*PthGrid;
    
    
    m = polyval(deriv,PthGrid);
    b = PalGrid - m.*PthGrid;
    
    Lins= zeros(etaNodes,2);
    for i=1:etaNodes
        Lins(i,1)=m(i);
        Lins(i,2)=b(i);
    end

    intersec=ones(length(b)-1,1);
    for i=1:length(b)-1
        intersec(i) = (b(i)-b(i+1))/(m(i+1)-m(i));
    end
    
    PthLinMin = [PthInt(1) intersec'];
    PthLinMax = [intersec' PthInt(3)];
    
    PthNodes = [PthInt(1) intersec' PthInt(3)];
    PalNodes = ones(size(PthNodes));
    for i=1:length(PthNodes)-1
        PalNodes(i) = PthNodes(i)*m(i)+b(i);
    end
    PalNodes(end) = PthNodes(end)*m(end)+b(end);
    
    X = linspace(Pth0-PthVar,Pth0+PthVar,1000);
    Y = X.*polyval(coeff,X);
    PalSpline = interp1(PthNodes,PalNodes,X);
    % abs error
    ae = abs(PalSpline-Y);
    % max abs error
    [maxae, maxind] = max(ae);
    % max rel error
    maxre = maxae/Y(maxind);
    % mean abs error
    meanae = mean(ae);
end