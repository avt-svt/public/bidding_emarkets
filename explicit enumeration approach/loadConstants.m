function [Pth0,delPRL,EthMax] = loadConstants()
    Pth0 = 90;
    delPRL = 10;
    EthMax = 0.25*48*Pth0;
end