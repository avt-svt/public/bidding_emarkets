function[PRLvec,PRLcell] = prlPartitions(PRLtot,PRLmin,PRLsplit)

    %% Calculate all possible partitions for PRLList
    idx = 0;
    for PRL = PRLtot
        for split = 1:PRLsplit
            plist = partitions(PRL,(PRLmin:PRL),[],split);
            if ~isempty(plist)
                idx = idx +1;
                pSize = size(plist,1);
                addRows = [zeros(pSize,PRLmin-1) plist zeros(pSize,max(PRLtot)-PRL)];
                if idx == 1
                    result = addRows;
                else
                    result = vertcat(result,addRows);
                end
            end
        end
    end
    result = unique(result,'rows','stable');
    PRLvec = find(~all(result==0));

    %% Transform partition vectors, e.g. from [2 0 0 2 0] to [1 1 4 4]
    cellCount = 0;
    PRLcell = {};
    multiplier = (1:PRLtot(end));
    for j = 1:size(result,1)
        row = result(j,:);
        portionsPRL = repelem(multiplier,row);
        portionsPRL = perms(portionsPRL);
        portionsPRL = unique(portionsPRL,'rows');
        for k = 1:size(portionsPRL,1)
            cellCount = cellCount + 1;
            PRLcell{cellCount,1} = sum(portionsPRL(k,:));
            PRLcell{cellCount,2} = portionsPRL(k,:);
            PRLcell{cellCount,3} = cumsum(portionsPRL(k,:));
            PRLcell{cellCount,4} = numel(portionsPRL(k,:));
        end
    end
    PRLcell = sortrows(PRLcell,4);
end