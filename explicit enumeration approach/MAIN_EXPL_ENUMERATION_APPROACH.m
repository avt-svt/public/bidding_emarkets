clear all
myPath = pwd;
tStart0 = tic;
addpath 'C:\GAMS\win64\28.2';

%% Choose parameters
% Load constants
[Pth0, delPRL, EthMax] = loadConstants();
% Choose delProdLoss from [0 0.5/100 1/100], default is 0.5/100
delProdLossList = [0.5/100];
% Choose ProdLossMode from ['q','l'], default is 'q'
ProdLossMode = 'q';
% Choose etaVar from [0 1/100 2/100], default is 1/100
etaVarList = [1/100];
% Choose perturbation ['normal','high','low']
perturbMode = 'normal';
% Choose first week and last week for scheduling
firstWeek = 1;
lastWeek = 4;
% Choose first week and last week for scheduling
firstWeekBidding = 1;
lastWeekBidding  = 4;
% Total PRL offered
PRLtot = [1:10];
% Smallest PRL offer quantity
PRLmin = 1;
% Max number of PRL offer split elements
PRLsplit = 3;
% conversion rate eta at Pth0 [t/MWh]
eta0 = 1/14;
% number of nodes for piece-wise linear function
%etaNodes = 3;
etaNodes = 15;
% Bidding mode ['knownPrices','estimPrices']
bidMode = 'estimPrices';
% Choose sigmFactor from [0.5, 1, 2], default is 1
sigmFactor = 1;
% fprintflay output settings
count = 0;
timeFormat = '%02d:%05.2f';
txtFormat = '%02d';
eta0Format = '%04.1f';
etaVarFormat = '%05.3f';

%gams relative optimality criterion
optcr=1e-03; 
threads=1;
%create output folder
if ~exist('1_GMS', 'dir')
   mkdir('1_GMS')
end
if ~exist('2_GDX_Output', 'dir')
   mkdir('2_GDX_Output')
end
%% initialize solution vectors
guel = @(s,v) strcat(s,strsplit(num2str(v)));
c.name = 'C';
c.form = 'full';
c.uels = {guel('',firstWeek:lastWeek)};
p.name = 'askPrice';

tsol.name = 'tsol';

for etaVar=etaVarList
for delProdLoss=delProdLossList
    count = 0;
%% Create model file
writeGmsAluModel();

%% Calculate production cost without PRL
tStart1 = tic;
PRL = 0;
fprintf(['\nBeginning run:             PRL=' sprintf(txtFormat,PRL) ', etaNodes=' sprintf(txtFormat,etaNodes) '\n']);
PthVar = 0.25*Pth0 - PRL;
gdxFileName = ['SchedBid_' perturbMode '_PRL=' sprintf(txtFormat,PRL) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];

% Create input data for scheduling
[set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
    param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
    param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
    param_AlP] = ...
    inputScheduling(perturbMode,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,'',delPRL,delProdLoss,ProdLossMode);

% Run gams scheduling
gams('AluModel',set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
    param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
    param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,...
    param_DAP,param_AlP);

% Get value C
c = rgdx('matsol.gdx',c);
C0 = c.val;
c = rmfield(c,'type');
c = rmfield(c,'dim');
c = rmfield(c,'val');

movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
movefile ('matdata.gms',['1_GMS' filesep 'SchedBidMatdata.gms']);
movefile ('matdata.gdx',['1_GMS' filesep 'SchedBidMatdata.gdx']);
t1 = toc(tStart1);
t0 = toc(tStart0);
fprintf(['Run solved in:             ' sprintf(timeFormat,floor(t1/60),rem(t1,60)) ' min.\n']);
fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t0/60),rem(t0,60)) ' min.\n']);

%% PRL partitions
PRLcheck = find(PRLmin > PRLtot);
if ~isempty(PRLcheck)
    error('PRLmin is larger than at least one element in PRLtot!');
end

[PRLvec,PRLcell] = prlPartitions(PRLtot,PRLmin,PRLsplit);
numLoops = length(PRLvec);
CPRL = zeros(numel(firstWeek:lastWeek),numLoops);

%% Loop
for PRL = PRLvec
    PthVar = 0.25*Pth0-PRL;
    if PRL>22.5
        error('PRL cannot exceed 22.5 MW!');
    end
    tStart2 = tic;
    count = count+1;
    gdxFileName = ['SchedBid' '_PRL=' sprintf(txtFormat,PRL) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode];
    fprintf(['\nBeginning run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) '):     PRL=' sprintf(txtFormat,PRL) ', etaNodes=' sprintf(txtFormat,etaNodes) '\n']);

    %% PRL=currentValue
    % Create input data for scheduling
    [set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
        param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
        param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,param_DAP,...
        param_AlP] = ...
        inputScheduling(perturbMode,firstWeek,lastWeek,Pth0,PthVar,eta0,etaVar,etaNodes,EthMax,PRL,'',delPRL,delProdLoss,ProdLossMode);

    % Run gams scheduling
    gams('AluModel',set_w,set_h,set_rec,set_lim,set_constr,set_i,param_Pth0,...
        param_PthVar,param_PRL,param_PRLProdLoss,param_eta0,param_meanae,...
        param_maxae,param_maxre,param_EthMax,param_m,param_b,param_PLinMin,param_PLinMax,...
        param_DAP,param_AlP);

    % Get value C
    c = rgdx('matsol.gdx',c);
    CPRL(:,count) = c.val;
    c = rmfield(c,'type');
    c = rmfield(c,'dim');
    c = rmfield(c,'val');
    movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
    movefile ('matdata.gms',['1_GMS' filesep 'SchedBidMatdata.gms']);
    movefile ('matdata.gdx',['1_GMS' filesep 'SchedBidMatdata.gdx']);

    t0=toc(tStart0);
    t2=toc(tStart2);
    fprintf(['Run (' sprintf(txtFormat,count) '/' sprintf(txtFormat,numLoops) ') solved in:     ' sprintf(timeFormat,floor(t2/60),rem(t2,60)) ' min.\n']);
    fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t0/60),rem(t0,60)) ' min.\n']);
end
fprintf('\nScheduling succesfully terminated.\n');

%% Bidding
for j = firstWeekBidding:lastWeekBidding
    tStart3 = tic;
    bidWeek = j;
    
    %% Create model file
    writeGmsBid(bidMode, optcr, threads);
    
    %% File name definition
    gdxFileName = ['BidSched_' perturbMode '_Bids=' sprintf(txtFormat,PRLsplit) '_w=' sprintf(txtFormat,bidWeek) '_sigm=' sprintf(txtFormat,sigmFactor) '_eta0^-1=' sprintf(eta0Format,1/eta0) '_etaVar=' sprintf(etaVarFormat,etaVar) '_n=' sprintf(txtFormat,etaNodes) '_delProdLoss=' sprintf(etaVarFormat,delProdLoss) '_' ProdLossMode '_' bidMode];
    idxBidding = find(firstWeek:lastWeek == bidWeek);
    % Create input data
    [set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,param_PRL,...
        param_CPRL,param_movAvg,param_f2] = inputBidding(C0,CPRL,PRLvec,PRLcell,...
        perturbMode, sigmFactor,bidWeek,idxBidding,bidMode);

    %% Run gams bidding
    fprintf(['\nCalculating bidding strategy ...\n']);
    
    %gamso.show = 'normal';
    if strcmp(bidMode,'knownPrices')
        gams('Bidding logOption=2',set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,...
            param_PRL,param_CPRL);
    elseif strcmp(bidMode,'estimPrices')
        gams('Bidding logOption=2',set_scenIdx,set_ii,set_map,param_mu,param_sigm,param_C0,...
            param_PRL,param_CPRL,param_movAvg,param_f2);
    end
    t3=toc(tStart3);
    tsol_temp = rgdx('matsol.gdx',tsol);
    etSolve = tsol_temp.val;
    sum_etSolve=sum(etSolve(:,2));
    fprintf(['Bidding solved in ' sprintf(timeFormat,floor(t3/60),rem(t3,60)) ' min.\n']);
    
    %% Clean up
    if exist('AluModel.gms','file')==2
    status=movefile ('AluModel.gms',['1_GMS' filesep 'AluModel.gms']);
    end
    if exist('AluModel.log','file')==2
    status=movefile ('AluModel.log',['1_GMS' filesep 'AluModel.log']);
    end
    if exist('AluModel.lst','file')==2
    status=movefile ('AluModel.lst',['1_GMS' filesep 'AluModel.lst']);
    end
    if exist('AluModel.lxi','file')==2
    status=movefile ('AluModel.lxi',['1_GMS' filesep 'AluModel.lxi']);
    end

    status=movefile ('Bidding.gms',['1_GMS' filesep 'Bidding.gms']);
%     if exist('Bidding.log','file')==2
%     status=movefile ('Bidding.log',['1_GMS' filesep 'Bidding.log']);
%     end
%     if exist('Bidding.lst','file')==2
%     status=movefile ('Bidding.lst',['1_GMS' filesep 'Bidding.lst']);
%     end
    if exist('Bidding.lxi','file')==2
    status=movefile ('Bidding.lxi',['1_GMS' filesep 'Bidding.lxi']);
    end
%     status=movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
    
    status=movefile ('matdata.gms',['1_GMS' filesep 'BidSchedMatdata.gms']);
    status=movefile ('matdata.gdx',['1_GMS' filesep 'BidSchedMatdata.gdx']);
    
    gdxFileName = ['Schaefer_Bids=' sprintf(txtFormat,PRLsplit) '_w=' sprintf(txtFormat,bidWeek) '_tol=' sprintf('%0.0e',optcr) '_threads=' sprintf('%d',threads) '_sumETSolve=' num2str(sum_etSolve,'%1.2f')];
    status=movefile ('matsol.gdx',['2_GDX_Output' filesep gdxFileName '.gdx']);
    listFileName = ['lst_Schaefer_Bids=' sprintf(txtFormat,PRLsplit) '_w=' sprintf(txtFormat,bidWeek) '_tol=' sprintf('%0.0e',optcr) '_threads=' sprintf('%d',threads) '_sumETSolve=' num2str(sum_etSolve,'%1.2f')];
    status=movefile ('Bidding.lst',['2_GDX_Output' filesep listFileName '.lst']);
    logFileName = ['log_Schaefer_Bids=' sprintf(txtFormat,PRLsplit) '_w=' sprintf(txtFormat,bidWeek) '_tol=' sprintf('%0.0e',optcr) '_threads=' sprintf('%d',threads) '_sumETSolve=' num2str(sum_etSolve,'%1.2f')];
    status=movefile ('Bidding.log',['2_GDX_Output' filesep logFileName '.log']); 

end
end
end
t4=toc(tStart0);
fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t4/60),rem(t4,60)) ' min.\n']);

%% Done
fprintf(['\nDONE!\n']);