function[GDXparameter] = createGDXparameter(name, val, form, dim, load, varargin)
    if ~ischar(name)
        error('Variable "NAME" has to be a string!');
    end
    GDXparameter.name = name;
    GDXparameter.val = val;
    GDXparameter.form = form;
    GDXparameter.type = 'parameter';
    GDXparameter.dim = dim;
    GDXparameter.load = load;
    if strcmp(load,'remove')
        GDXparameter = rmfield(GDXparameter,'load');
    end
    if nargin > 5
        if ~isrow(varargin{1})
            error('Only row vectors allowed for unique element list!');
        end
        uelnumb = size(varargin{1},2);
        for i = 1:uelnumb
            GDXparameter.uels{1,i} = varargin{1}{i};
        end
    end
end