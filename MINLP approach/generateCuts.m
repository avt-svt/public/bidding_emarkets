function [set_c,param_uB,param_ug] = generateCuts(ug,uB)
%create Matlab-structures needed for the additional constraints due to
%Benders optimality cuts

guel = @(s,v) strcat(s,strsplit(num2str(v)));

set_c = createGDXset('c',{guel('',1:length(ug))},'initialize');

param_uB = createGDXparameter('uB', uB, 'full', 1, 'initialize',[set_c.uels]);
param_ug = createGDXparameter('ug', ug, 'full', 1, 'initialize',[set_c.uels]);
end

