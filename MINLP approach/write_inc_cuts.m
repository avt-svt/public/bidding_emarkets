function [] = write_inc_cuts()
%create include-file to include the Benders optimality cuts in the
%Bidding_Master.gms problem

fileID = fopen('cuts.inc','wt');

fprintf(fileID,'Equations \n');
fprintf(fileID,'Cuts(bid,c) \n; \n');
fprintf(fileID,'Cuts(bid,c) .. CPRL(bid) =g= ug(c) - uB(c) * sum(biid,PRL(biid)*combinations(biid,bid));\n');

fclose(fileID);
end

