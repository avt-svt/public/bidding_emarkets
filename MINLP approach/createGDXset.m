function[GDXset] = createGDXset(name, uels, load, varargin)
%auxiliary function to create matlab-structures that fit GAMS-Syntax
    if ~ischar(name)
        error('Variable "NAME" has to be a string!');
    end
    if ~isrow(uels)
        error('Only row vectors allowed for unique element list!');
    end
    GDXset.name = name;
    GDXset.type = 'set';
    uelnumb = size(uels,2);
    for i = 1:uelnumb
        GDXset.uels{1,i} = uels{i};
    end
    GDXset.load = load;
    if strcmp(load,'remove')
        GDXset = rmfield(GDXset,'load');
    end
    if nargin > 3
        GDXset.val = varargin{1};
    end
end