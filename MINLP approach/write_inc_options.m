function [] = write_inc_options(gams_tolerance,threads)
%create include-file to set tolerance and thread options in the
%Bidding_Master.gms problem

fileID = fopen('options.inc','wt');
fprintf(fileID,'\noption optcr = ');
fprintf(fileID,num2str(gams_tolerance),'\n');
fprintf(fileID,'\noption threads = ');
fprintf(fileID,num2str(threads),'\n');
fclose(fileID);
end