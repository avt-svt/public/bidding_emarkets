function [DAP,AlP] = xlsSched(Week)
% extract information for the scheduling problem from the excel file
% 'PriceData'
    file = ['PriceData.xlsx'];
    dapRange = 'A2:G25';
    otherRange = 'H2:J2';
    sheet = ['Week',int2str(Week)];
    dapX = readmatrix(file,'Sheet',sheet,'Range',dapRange);
    otherX = readmatrix(file,'Sheet',sheet,'Range',otherRange);
    dapX = reshape(dapX,1,168);

    DAP = dapX;
    other = otherX;
    AlP = other(:,3);
end