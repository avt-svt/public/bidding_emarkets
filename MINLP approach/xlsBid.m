function [muPRL,sigPRL,muLogF1,sigmLogF1,movAvg,f2] = xlsBid(chosenWeek)
% extract information for the bidding problem from the excel file
% 'PriceData'
    file = 'PriceData.xlsx';
    muPRLrange = 'I2';
    otherRange = 'K2:N2';
    sheet = ['Week',int2str(chosenWeek)];
    muPRL = readmatrix(file,'Sheet',sheet,'Range',muPRLrange);
    sigPRL = 0.1*muPRL;
    other = readmatrix(file,'Sheet',sheet,'Range',otherRange);
    muLogF1 = other(1);
    sigmLogF1 = other(2);
    movAvg = other(3);  
    f2 = other(4);
end