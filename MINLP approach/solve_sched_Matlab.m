function [x_sol_dual,fval_dual]=solve_sched_Matlab(PRL,d,B,D,g)
% Solving the dual of the scheduling sub problem for given PRL(=x)

% primal problem: min d'y
%                s.t. Bx + Dy >= g, y >= 0
%
% dual problem:   max u'(g-Bx)
%                s.t. D'u >= d', u >= 0

x=[PRL;0];
%primal_lb=zeros(1,506);
dual_lb=zeros(1,3375);

options = optimset('linprog');
options.Display = 'off';

%primal
primal_A=-1*D;
primal_b=-1*(g-B*x);
primal_f=d;
% [x_sol,fval,exit,out]=linprog(primal_f,primal_A,primal_b,[],[],primal_lb,[]);

%dual
dual_A = -1*primal_A';
dual_b = primal_f;
dual_f = primal_b;
[x_sol_dual,fval_dual,exit_dual,out_dual]=linprog(dual_f,dual_A,dual_b,[],[],dual_lb,[],options);
if exit_dual~=1
    error('linear scheduling subproblem did not converge')
end
fval_dual=-fval_dual;

end