function [m,b] = lin_Prodloss(maxPRL,steps,eta0,Pth0,delProdLoss)
%computes a quadratic fit for the production losses and gives a linearization

PRL=linspace(0,maxPRL,steps)';

PRLIntvll = [-maxPRL 0 maxPRL];
ProdLossIntvll = [eta0*Pth0*delProdLoss 0 eta0*Pth0*delProdLoss];
coeff = polyfit(PRLIntvll,ProdLossIntvll,2);
deriv = [2*coeff(1), coeff(2)];
PRLProdLoss = polyval(coeff,PRL);
m = polyval(deriv,PRL);
b = PRLProdLoss - m.*PRL;
end

