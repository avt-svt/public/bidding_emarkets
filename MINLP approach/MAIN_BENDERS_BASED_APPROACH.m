clear all
addpath 'C:\GAMS\win64\28.2';
tStart0 = tic;

%% Setup
%Parameters
Pth0 = 90;             %mean Power 90MW
maxPRL = 10;           %possible PLR
EthMax = 0.25*48*Pth0; %maximum energy storage capacity
eta0 = 1/14;           %conversion rate eta at Pth0 [t/MWh]
etaNodes = 15;         %number of nodes for piece-wise linear alu production function
PthVar = 0.25*Pth0;    %define Maximum Flexibility 

% Choose delProdLoss from [0 0.5/100 1/100], default is 0.5/100
delProdLoss = [0.5/100];

% Choose etaVar from [0 1/100 2/100], default is 1/100
etaVar = [1/100];

% Choose weeks for scheduling and bidding. Possible are: 1,2,3,4
Week_List = [1,2,3,4];

% Choose Number of PRL offer split elements. Possible are: 1,2,3
Bids_List = [1,2,3];

% Choose sigmFactor from [0.5, 1, 2], default is 1
sigmFactor = 1;

%choose GAMS options
gams_tolerance=1e-3; % realtive optimality gap 'optcr'
threads=1;           % number of threads used

% fprintflay output settings
timeFormat = '%02d:%05.2f';
txtFormat = '%02d';
eta0Format = '%04.1f';
etaVarFormat = '%05.3f';

%% Preparation
%create output folder
if ~exist('GDX_Output', 'dir')
   mkdir('GDX_Output')
end

NumberOfCuts=maxPRL+1; %number of benders cuts that will be computed. from PRL=0 to PRL=maxPRL
PRLtot = [0:maxPRL];   %vector of all possible values for PRL

% Get linearized formulation of production loss due to PRL (quadratic)
% (instaed of computing a linearization, that is valid for all PRL, you could compute ProdLoss=f(PRL) in every cycle in the PRL=PRLtot loop) 
lin_steps=maxPRL+1; %esures that linearization is 100% accurate for the integer values 0:maxPRL
[m_Prodloss,b_Prodloss]=lin_Prodloss(maxPRL,lin_steps,eta0,Pth0,delProdLoss);

%initialize matrices
ug=zeros(NumberOfCuts,4);
uB=zeros(NumberOfCuts,4);
CPRL=zeros(11,4);
C0=zeros(4,1);
maxCPRL=zeros(4,1);

%% Solve the linear scheduling problem to generate Cuts
for Week=Week_List
    tStartCuts=tic;
    [DAP,AlP] = xlsSched(Week); %DAP = day ahead price, AlP = Aluminium Price
    
    %solve scheduling for every possible PRL
    for PRL=PRLtot 
        [m,b] = lin_AluProduction(Pth0,PthVar-PRL,eta0,etaVar,etaNodes); %linearization of aluminium production depending on the power input
        [d,B,D,g] = make_decompSchedProb(eta0,Pth0,PthVar,EthMax,DAP,AlP,m,b,m_Prodloss,b_Prodloss);
        [x_sol_dual,fval_dual]=solve_sched_Matlab(PRL,d,B,D,g);
        CPRL(PRL+1,Week)=fval_dual;
        u=x_sol_dual;
        % Computing matrices for Benders optimality cuts: CPRL >= u'(g - Bx) = u'g -u'B*x
        ug(PRL+1,Week)=u'*g;
        uB(PRL+1,Week)=u'*B(:,1);
    end
    
    C0(Week)=CPRL(1,Week); % Costs for PRL==0
    maxCPRL(Week)=CPRL(end,Week); %maximum possible cost because of minimal flexibility   
    t1=toc(tStartCuts);
    fprintf(['Cuts for Week:' num2str(Week) ' created in ' num2str(t1,'%.2f') ' sec.\n']);
end

%% Solving the MINLP Bidding Master problem via GAMS + BARON
for maxBids=Bids_List
    fprintf(['Bidding will begin for: ' num2str(maxBids) ' Bid(s)\n']);
    
    for Week=Week_List
        tStartWeek = tic;

        %Reset inc-files
        f = fopen('cuts.inc','w');  
        fclose(f);
        f = fopen('options.inc','w');  
        fclose(f);
        
        %write inc-files
        write_inc_cuts
        write_inc_options(gams_tolerance,threads);    

        %create input structures
        [set_bid, param_mu, param_sigm, param_movAvg, param_f2, param_C0, param_combinations, param_maxPRL, param_maxCPRL] = ...
            inputGAMS(sigmFactor, Week,C0(Week),maxBids,maxPRL,maxCPRL(Week));
        [set_c,param_uB,param_ug] = generateCuts(ug(:,Week),uB(:,Week));
        
        %call GAMS
        gams('Bidding_Master logOption=2 ',set_c,param_uB,param_ug,set_bid,param_mu, param_sigm, param_movAvg, param_f2, param_C0, param_combinations, param_maxPRL, param_maxCPRL);    

        %get the etSolve time from the output gdx-file
        tsol.name='etSolve';
        tsol_temp = rgdx('matsol.gdx',tsol);
        etSolve=tsol_temp.val;

        t2=toc(tStartWeek);

        %move result files to output-folder
        gdxFileName = ['Benders_allmb_Bids=' sprintf(txtFormat,maxBids) '_w=' sprintf(txtFormat,Week) '_tol=' sprintf('%0.0e',gams_tolerance) '_threads=' sprintf('%d',threads) '_ETSolve=' num2str(etSolve,'%1.2f') 'sec'];
        status1=movefile ('matsol.gdx',['GDX_Output' filesep gdxFileName '.gdx']);
        status2=movefile ('Bidding_Master.lst',['GDX_Output' filesep 'lst_' gdxFileName '.lst']);
        status3=movefile ('Bidding_Master.log',['GDX_Output' filesep 'log_' gdxFileName '.log']);     

        fprintf(['Week ' sprintf(txtFormat,Week) ': Bidding solved in ' num2str(t2,'%.2f') ' sec.\n']);
    end
end

%% Fin
t_total=toc(tStart0);
fprintf(['\nDONE!\n']);
fprintf(['Elapsed time since launch: ' sprintf(timeFormat,floor(t_total/60),rem(t_total,60)) ' min.\n']);














