$set matout "'matsol.gdx'";
display "%system.filesys%/";
file screen /'con'/;

$macro erf(x) (1/2 + 1/sqrt(2*pi)*exp(-power(x,2)/2)*(x + power(x,3)/3 + power(x,5)/15 + power(x,7)/105 + power(x,9)/945 + power(x,11)/10395 + power(x,13)/135135 + power(x,15)/2027025 + power(x,17)/34459425 + power(x,19)/654729075 + power(x,21)/13749310575 + power(x,23)/316234143225))

Sets
  bid
  h              Hours in a week
  i              Intervalls for linearization
  c              Number of cuts

*  scenIdx         Loop index for scenarios
*  stat            Set for model and solver status /modelstat,solvestat/
;


Scalars
  mu                   Median of factor f1's natural logarithm [-]
  sigm                 Standard deviation of factor f1's natural logarithm [-]
  movAvg               Moving average of past ask prices [EUR]
  f2                   Price estimation factor f2
;

Parameters
  uB(c)
  ug(c)
  combinations(bid,bid)
*  returnStat(scenIdx,stat) Parameter for model and solver status
*  tSol(scenIdx)            Time used by solver [CPU secs]
  C0                       Production cost without PRL [EUR]
  maxPRL
  maxCPRL
  solvetime
  barontime
;

$if exist matdata.gms $include matdata.gms

Alias(bid,biid)

Variables
  CPRL(bid)                    Total cost [EUR]

*  askPrice(bid)                Price for current PRL offer
  x(bid)                       Transformed ask price
  y1(bid)                      Natural logarithm of price estimation factor f1
*  pRefuse                 Probabilty that entire offer package is refused
*  p(bid)                       Probabilty that bid package i is accepted
  Ctot                    Weekly cost in current loop [EUR]
  C_b(bid)
;

Positive variables
  pRefuse
  p(bid)
  askPrice(bid)
;

Integer Variable
  PRL(bid)              PRL partition for given scenario [MW]
;

Equations
  CostWeek         Weekly production cost
*  CostBid(bid)
  Transf(bid)           Transform offer price into standard normally distributed variable
  ProbRefuse       Probability of entire offer refusal
  ProbAcc(bid)          Probability of partial offer acceptance
  ProbAccLast(bid)
  Estimation(bid)       Linking factors to asked price
  ascendingBids(bid)
  sumPRL


;

*  CostWeek                          ..  Ctot =e= pRefuse*C0 + sum(bid,p(bid)*C_b(bid));
*  CostBid(bid)                      ..  C_b(bid) =e= CPRL(bid) - sum(biid,PRL(biid)*askPrice(biid)*combinations(biid,bid));
  CostWeek                          ..  Ctot =e= pRefuse*C0 + sum(bid,p(bid)*(CPRL(bid) - sum(biid,PRL(biid)*askPrice(biid)*combinations(biid,bid))));
  Transf(bid)                       ..  x(bid) =e= (y1(bid) - mu)/(sigm);
  Estimation(bid)                   ..  y1(bid) =e= log(askPrice(bid)) - log(movAvg) - log(f2);

  ProbRefuse                            ..  pRefuse =e= 1 - sum(bid,p(bid));
  ProbAcc(bid)$(ord(bid)<card(bid))     ..  p(bid) =e= erf(x(bid+1))-erf(x(bid));
  ProbAccLast(bid)$(ord(bid)=card(bid)) ..  p(bid) =e= 1-erf(x(bid));

  ascendingBids(bid)$(ord(bid)>1)   ..  askPrice(bid) =g= askPrice(bid-1);
  sumPRL                            ..  sum(bid,PRL(bid)) =l= maxPRL;

  x.lo(bid)             = -2;
  x.up(bid)             = 2;
*  askPrice.lo(bid)      = 1000;
*  askPrice.up(bid)      = 6000;
  askPrice.lo(bid)      = 0;
  askPrice.up(bid)      = movAvg*3;
  y1.lo(bid)            = mu - 2*sigm;
  y1.up(bid)            = mu + 2*sigm;
  p.lo(bid)             = 0;
  p.up(bid)             = 1;
  pRefuse.lo            = 0;
  pRefuse.up            = 1;
  CPRL.lo(bid) = C0;
  CPRL.up(bid) = maxCPRL;
  PRL.lo(bid) = 0;
  PRL.up(bid) = maxPRL;
  Ctot.lo = C0 - movAvg*3*maxPRL;
  Ctot.up = C0;
*  C_b.lo(bid) = C0 - movAvg*3*10;
*  C_b.up(bid) = maxCPRL;

*Init
  p.l(bid)                  = 0.3;
  askPrice.l(bid)           = movAvg;
  y1.l(bid)                 = - log(f2);
  CPRL.l(bid)               = (C0+maxCPRL)/2;
  PRL.l(bid)                = 3;




$include cuts.inc

Model    bidding    / all /;

option MINLP = Baron
       reslim = 14400
       threads = 1
       optca = 0.001
       optcr = 1e-5;
*$onecho > baron.opt
*DeltaTerm 1
*DeltaT 600
*DeltaA 0.5
*$offecho

$include options.inc

solve bidding minimizing Ctot using minlp;

*Set stat / modelStat, solveStat /;

Parameter etSolve;
etSolve = bidding.etSolve;

execute_unload %matout%;
