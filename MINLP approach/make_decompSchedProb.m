function [d,B,D,g] = make_decompSchedProb(eta0,Pth0,PthVar,EthMax,DAP,AlP,m,b,m_ProdLoss,b_ProdLoss)
% scheduling problem in standard form with variables decomposed
% to master variables x and scheduling variables y;
% min d'y 
% s.t. 
% Bx + Dy >= g, x >= 0, y >= 0

nh=168;  %hours in a week
nY=3*nh+1+1;  %number of scheduling Variables

d=[DAP';zeros(2*nh,1);AlP;0];

%start_th
D1=zeros(2,nY);
g1=zeros(2,1);
D1(1,2*nh+1)=1;
D1(2,2*nh+1)=-1;
g1(1,1)=EthMax;
g1(2,1)=-EthMax;

%Storage_th
D2_1=[eye(167),zeros(167,1),zeros(167,nh),eye(167),zeros(167,1),zeros(167,1),zeros(167,1)];
D2_1=D2_1+[zeros(167,2*nh+1),-1*eye(167),zeros(167,1),zeros(167,1)];
g2_1=ones(167,1)*Pth0;
D2_2=-1*D2_1;
g2_2=-1*g2_1;

%Reconc
D3=zeros(2,nY);
D3(1,nh)=1;
D3(1,3*nh)=1;
D3(2,nh)=-1;
D3(2,3*nh)=-1;
g3=[Pth0+EthMax;-Pth0-EthMax];

%LinProd
D4=zeros(nh*15,nY);
g4=zeros(nh*15,1);
for h=1:nh
    D4([h*15-14:h*15],h)=m;
    D4([h*15-14:h*15],nh+h)=-1*ones(15,1);
    D4([h*15-14:h*15],3*nh+2)=-1*ones(15,1);  
    g4([h*15-14:h*15],1)=-b;
end

%AluTarget
D5_1=[zeros(1,nh),ones(1,nh),zeros(1,nh),1,0];
D5_2=-1*D5_1;
g5_1=nh*Pth0*eta0;
g5_2=-nh*Pth0*eta0;

%PowerIn Bounds
B6_1=[-1*ones(nh,1),zeros(nh,1)];
D6_1=[eye(nh),zeros(nh,2*nh+1+1)];
B6_2=B6_1;
D6_2=-1*D6_1;
g6_1=ones(nh,1)*(Pth0-PthVar);
g6_2=ones(nh,1)*(-Pth0-PthVar);

%ThermStorage Bounds (careful: is now between 0 and 2*StorageMax)
D7=[zeros(nh,2*nh),-1*eye(nh),zeros(nh,2)];
g7=ones(nh,1)*(-2)*EthMax;

%new formulation of ProdLoss by linearization
B8=[-m_ProdLoss,zeros(11,1)];
D8=[zeros(11,3*nh+1),ones(11,1)];
g8=[b_ProdLoss];

%add everything together
B=[zeros(2860,2);B6_1;B6_2;zeros(nh,2);B8];
D=[D1;D2_1;D2_2;D3;D4;D5_1;D5_2;D6_1;D6_2;D7;D8];
g=[g1;g2_1;g2_2;g3;g4;g5_1;g5_2;g6_1;g6_2;g7;g8];
end