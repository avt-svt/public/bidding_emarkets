function [m,b] = lin_AluProduction(Pth0,PthVar,eta0,etaVar,etaNodes)
%compute m (slope) and b (axis section) to linearize aluminium production as a function of PowerInput
    PthInt = [Pth0*(1-0.25), Pth0, Pth0*(1+0.25)];
    etaMin = eta0*(1-etaVar/(1+etaVar));
    eta = [etaMin, eta0, etaMin];
    coeff = polyfit(PthInt,eta,2);

    deriv = [3*coeff(1), 2*coeff(2), coeff(3)];
    
    PthGrid = linspace(Pth0-PthVar,Pth0+PthVar,etaNodes);
    PthGrid = movmean(PthGrid,[0 1]);
    PthGrid = PthGrid(1:end-1);
    PthGrid = [PthGrid(1:end/2) Pth0 PthGrid(end/2+1:end)];
    PalGrid = polyval(coeff,PthGrid).*PthGrid;
    m = polyval(deriv,PthGrid);
    b = PalGrid - m.*PthGrid;
end