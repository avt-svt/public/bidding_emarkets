function [set_bid, param_mu, param_sigm, param_movAvg, param_f2,param_C0, param_combinations, param_maxPRL, param_maxCPRL] = inputGAMS(sigmFactor,bidWeek,C0,maxBids,maxPRL,maxCPRL)
%create matlab-structures filled with the input data needed for solving the
%bidding problem

guel = @(s,v) strcat(s,strsplit(num2str(v)));
%% Bidding
    [muPRL,sigPRL,muLogF1,sigmLogF1,movAvg,f2] = xlsBid(bidWeek);
    
    mu = muLogF1;
    sigm = sigmLogF1*sigmFactor;
    comb=triu(ones(maxBids));
    
%% Input data
%%%
set_bid = createGDXset('bid',{guel('',1:maxBids)},'initialize');

param_mu = createGDXparameter('mu', mu, 'full', 0, 'initialize');
param_sigm = createGDXparameter('sigm', sigm, 'full', 0, 'initialize');
param_movAvg = createGDXparameter('movAvg',movAvg,'full',0,'initialize');
param_f2 = createGDXparameter('f2',f2,'full',0,'initialize');
param_C0 = createGDXparameter('C0', C0, 'full', 0, 'initialize');
param_combinations = createGDXparameter('combinations', comb, 'full', 2, 'initialize',[set_bid.uels,set_bid.uels]);
param_maxPRL = createGDXparameter('maxPRL', maxPRL, 'full', 0, 'initialize');
param_maxCPRL = createGDXparameter('maxCPRL', maxCPRL, 'full', 0, 'initialize');
end

